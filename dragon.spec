Name:		xdragon
Version:	1.1.1
Release:        1
Summary:	Simple drag-and-drop source/sink for X

License:	GPLv3+
URL:		https://gitlab.com/jeremieglbd/dragon/
Source0:	https://gitlab.com/jeremieglbd/dragon/-/archive/1.1.1/dragon-1.1.1.tar.gz

#BuildRequires:
Requires: gtk3-devel

%description

%global debug_package %{nil}

%prep
%setup


%build
make


%install
make PREFIX=%{buildroot}/usr/local/bin/ install

%check


%files
%license
%doc
/usr/local/bin/xdragon

%changelog

